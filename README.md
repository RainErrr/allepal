#Assigment

Implement responsive design for both desktop and mobile view as per given design. Make api call to given endpoint and fetch active item data- display that data. As an extra task user can input item id and see the result on screen.

##Installation

* Make sure you have at least node 15 and relevant npm
* `git clone <project>` to clone project
* `npm i` for node modules

##Development:

###Run UI
`npm run dev`

##Stack
* Typescript
* ReactJS
* Vite

##Remarks

* Due to design markup being an image file it was hard to determine exact layout.
* Responsiveness is only for mobile and desktop, layout has not been done for tablets or flipped mobile devices as no mention of it was in the description of the task.
* Last bidder information is hardcoded, due to no info about it the API response.
* The project is not build ready as "vite" would require some more setup.
* The marked data structure in API response does not match the actual response.
* All in all enjoyable task :)