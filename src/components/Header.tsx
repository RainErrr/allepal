import React, {ReactElement} from 'react'

const Header: React.FC = (): ReactElement => {
  return (
    <div className="header-wrapper">
      <div className="section">
        <img className="d-none d-sm-block" src="/src/assets/images/arrow.png" alt=""/>
      </div>
    </div>
  )
}

export default Header