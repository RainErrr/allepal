import React, {ReactElement} from 'react'

const Footer: React.FC = (): ReactElement => {
  return (
    <div className="footer-wrapper">
      <div className="section section-top">
        <div className="container d-flex flex-column flex-sm-row align-items-center justify-content-center">
          <div>
            <div className="row pb-sm-4">
              <div className="col-12">
                <h1>WANT HIGHER?</h1>
                <div className="text-muted mt-4">
                  Before these experiments most physicists believed in an objective, independent universe.
                  They still clung to the assumption that physical states exist in some absolute sense before they are measured.
                </div>
              </div>
            </div>
            <div className="row pt-5">
              <div className="col-12 col-sm-6">
                <h1>CONTACT US</h1>
                <div className="text-muted mt-3 mt-sm-4">Before these experiments most</div>
                <div className="text-muted mt-3">
                  <img src="/src/assets/images/envelope.png" alt=""/>
                  <span className="ms-3">info@allepal.ee</span>
                </div>
                <div className="text-muted mt-3">
                  <img src="/src/assets/images/phone.png" alt=""/>
                  <span className="ms-3">+372 65656565</span>
                </div>
              </div>
              <div className="col-12 pt-5 pt-sm-0 col-sm-6">
                <h1>LOCATION</h1>
                <div className="text-muted mt-sm-4">AllePal OÜ</div>
                <div className="text-muted">Maakri 23a</div>
                <div className="text-muted">10145 Tallinn, Eesti</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="section section-bottom"/>
    </div>
  )
}

export default Footer