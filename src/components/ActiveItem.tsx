import React, {ReactElement, useEffect, useState} from 'react'

type ActiveItem = {
  buynowPriceEur: number,
  currentBids: number,
  currentPriceEur: number,
  dateEnd: string,
  imageUrl: string
  itemId: number,
  title: string,
}

const ActiveItem: React.FC = (): ReactElement => {
  const [activeItem, setActiveItem] = useState<ActiveItem | null>(null)
  const [activeItemId, setActiveItemId] = useState<string>('157548368')

  const loadActiveItem = async (id: string) => {
    const url = `https://api.osta.ee/api/items/active/${id}`
    const item = await fetch(url).then(response => response.ok ? response.json() : Promise.reject('Request failed')).catch(error => {
      console.log(error)
      setActiveItem(null)
    })
    setActiveItem(item)
  }

  useEffect(() => {
    loadActiveItem(activeItemId)
  }, [activeItemId])

  const onActiveItemIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    setActiveItemId(() => (event.target.value))
  }

  return (
    <>
      <div className="container d-flex flex-column align-items-center justify-content-center">
        <h3 className="text-muted text-center mb-2 mb-sm-4 mt-2">Viimati lõppenud</h3>
        <div className="input-group my-2 w-50 d-flex align-items-center">
          <label className="text-muted me-4">Toode</label>
          <input placeholder="Insert item Id" className="form-control" type="text" value={activeItemId}
                 onChange={onActiveItemIdChange}/>
        </div>
        {activeItem && (
          <div className="d-sm-flex mt-2 mt-sm-4">
            <div className="col-12 col-sm-3 d-flex justify-content-center justify-content-sm-end">
              <img src={activeItem.imageUrl} alt=""/>
            </div>
            <div className="col-12 col-sm-9 d-flex flex-column align-items-center justify-content-center pt-2">
              <div className="fw-bold fs-3 fs-sm-1 text-center">{activeItem.title}</div>
              <table className="w-100 mt-2 mt-sm-5">
                <tbody>
                <tr>
                  <td className="fs-3 fs-sm-1 fw-bold orange text-center">{activeItem.currentPriceEur}</td>
                  <td className="fs-3 fs-sm-1 fw-bold orange text-center">Laanekene</td>
                  <td className="fs-3 fs-sm-1 fw-bold orange text-center">{activeItem.currentBids}</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                  <td className="text-muted text-center">hetke hind</td>
                  <td className="text-muted text-center">juhtiv pakkuja</td>
                  <td className="text-muted text-center">pakkumist</td>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        )}
      </div>
    </>
  )
}
export default ActiveItem