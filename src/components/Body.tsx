import React, {ReactElement} from 'react'
import ActiveItem from './ActiveItem'

const Body: React.FC = (): ReactElement => {
  return (
    <div className="body-wrapper">
      <div className="section section-top d-flex d-block d-sm-none align-items-center">
        <div className="container">
          <h1>IS THE WEB POSSIBLE WITHOUT THE SPIDER?</h1>
          <div className="mt-4">Are space and time physical objects that would   continue to exits even if living creatures were removed from the scene?</div>
          <div className="mt-4">Figure out the nature of the world has obsessed scientists and philosophers for millennia. Three hundred years ago, the Irish empiricist Geroge Berkeley contributed a particularly prescient observation: The only thing can perceive are out perceptions.</div>
        </div>
      </div>
      <div className="section-middle">
        <ActiveItem/>
      </div>
      <div className="section section-bottom">
        <div className="container d-flex flex-column flex-sm-row align-items-center justify-content-center">
          <div className="col-12 col-sm-6 me-sm-4">
            <h1>MESSING WITH THE LIGHT</h1>
            <ul className="mt-5">
              <li className="mb-5">Quantum mechanics is the physicist's most accurate model for describing the worlds of the atom.</li>
              <li className="mb-5">But it also makes some of the most persuasive arguments that conscious perception is integral to the workings of the universe.</li>
              <li className="mb-5">Quantum theory tells us that an unobserved small object</li>
            </ul>
          </div>
          <div className="col-12 col-sm-6 ms-2 ms-sm-4 mt-5 mt-sm-0">
            <h2 className="mb-5">WHAT ACCOMPLISHES THIS COLLAPSE?</h2>
            <p className="mb-5">What accomplishes this collapse? Messing with it. Hitting it with a bit of light in order to take its picture. Just looking at it does the job.</p>
            <p>Experiments suggest that mere knowledge in the experimenter's mind is sufficient to collapse a wave function and convert possibility to reality.</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Body