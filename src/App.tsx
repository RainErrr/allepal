import React, {ReactElement} from 'react'
import Header from './components/Header'
import Body from './components/Body'
import Footer from './components/Footer'

const App: React.FC = (): ReactElement => {
  return (
    <>
      <Header/>
      <Body/>
      <Footer/>
    </>
  )
}

export default App
